const tokenInterceptor = (req, res, next) => {
  let token = req.headers['token'];

  if(!token)
    return res.status(401).json({code: -1, message: "Sem token"});

  if(token != '123456')
    return res.status(401).json({code: -1, message: "Token invalido"});

  next();
}

module.exports = tokenInterceptor;