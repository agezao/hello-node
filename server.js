const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const mongoose = require('mongoose');

//
const db = mongoose.connect('mongodb://127.0.0.1/node102', { useMongoClient: true })
  .then(status => {
    console.log('Connected mongo')
  });
//
let app = express();

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.use(morgan('dev'));

// Interceptors

const tokenInterceptor = require('./interceptors/token.interceptor');
app.use(tokenInterceptor);

//

const routesPrivadas = require('./routes');
app.use('/api', routesPrivadas);


app.listen(7788, function(){
  console.log('Server at ' + 7788);
});