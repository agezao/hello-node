const express = require('express');
const Person = require('../models/persons.model');

const router = express.Router();

// -> /api/persons
router.route('/')
  .get(function(req, res) {
    Person.find()
      .then(persons => {
        res.json(persons);
      });
  })
  .post(function(req, res) {
    if(!req.body.name)
      return res.status(400).json('Falto nome');

    let person = new Person(req.body);
    person.save()
      .then(dbPerson => {
        res.json(dbPerson);
      })
      .catch(err => {
        res.json(err);
      });
  });

// -> /api/persons/:idPessoa
router.route('/:idPessoa')
  .get(function(req, res) {
    Person.findOne({ _id: req.params.idPessoa })
      .then(person => {
        res.json(person);
      })
  })
  .delete(function(req, res) {
    Person.remove({_id: req.params.idPessoa})
      .then(res => {
        res.json(res);
      })
      .catch(err => {
        res.json(err);
      });
  })
  .put(function(req, res) {
    let updateObject = {};

    if(req.body.name)
      updateObject.name = req.body.name;
    if(req.body.password)
      updateObject.password = req.body.password;

    if(updateObject == {})
      return res.status(400).json(false);

    Person.update({ _id: req.params.idPessoa }, {$set: updateObject})
      .then(status => {
        res.json(status);
      })
      .catch(err => {
        res.json(err);
      });
  });

module.exports = router;
