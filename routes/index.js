const express = require('express');
const homeRoute = require('./home');
const personsRoute = require('./persons');

const router = express.Router();

router.use('/', homeRoute);
router.use('/persons', personsRoute);

module.exports = router;